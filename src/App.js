import logo from './logo.svg';
import gubka from './gubka.gif';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={gubka} width={300} height={250}/>
        <p>
           Ноготочки от коллег
        </p>
      </header>
    </div>
  );
}

export default App;
